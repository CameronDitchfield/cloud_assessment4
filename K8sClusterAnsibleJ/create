#!/bin/bash

ANSIBLEENV=$1

if [[ -z $ANSIBLEENV ]]
then
  echo "Not given parameter ANSIBLEENV (must be dev or prod)"
  exit 1
fi

#Create virtual environment
pip install virtualenv
python -m virtualenv pyvenv
source ./pyvenv/bin/activate
pip install -r requirements.txt

#Grab Kubespray repo
git clone https://github.com/kubernetes-sigs/kubespray
cd kubespray
git checkout v2.12.0
git checkout v2.9.0 -b v2.9.0new

#Set the necessary variables

source ../../var/shc.tfvars
KeyFile="user_${env}.pem"
export ANSIBLEENV bucket_name KeyFile

vpc_id=$(aws s3 cp s3://$bucket_name/env:/${env}/infra/terraform.tfstate - | jq -r '.outputs.vpc_id.value')
vpc_sg_id=$(aws s3 cp s3://$bucket_name/env:/${env}/infra/terraform.tfstate - | jq -r '.outputs.vpc_sg_id.value')
priv_subnet_id1=$(aws s3 cp s3://$bucket_name/env:/${env}/infra/terraform.tfstate - | jq -r '.outputs.priv_subnet_id.value[]')
priv_subnet_id2=$(aws s3 cp s3://$bucket_name/env:/${env}/infra/terraform.tfstate - | jq -r '.outputs.priv_subnet_id2.value[]')
pub_subnet_id=$(aws s3 cp s3://$bucket_name/env:/${env}/infra/terraform.tfstate - | jq -r '.outputs.pub_subnet_id.value[]')
kube_sg_name=$(aws s3 cp s3://$bucket_name/env:/${env}/infra/terraform.tfstate - | jq -r '.outputs.kube_sg_name.value')

#Grab the key
cd ..
aws s3 cp s3://${bucket_name}/keypairs/${KeyFile} /var/lib/jenkins/${KeyFile}
chmod 600 /var/lib/jenkins/${KeyFile}

# Check ANSIBLEENV is not a path
if ! echo "$ANSIBLEENV" | grep '/' >/dev/null 2>&1
then
	ANSIBLEENV="$PWD/environments/$ANSIBLEENV"
fi

# This line supports the use of Virtual Environments to use the correct python while building ec2
export PYTHON=$(which python)

ansible-playbook -i $ANSIBLEENV --extra-vars "ansible_ssh_private_key_file="/var/lib/jenkins/${KeyFile}" chosen_region=$region teamname=$teamname dns_zone=$dns_domain project_name=$project \
owner_name=$owner start_date=$start_date end_date=$end_date zone_id=$zone_id vpcid=$vpc_id vpcsg=$vpc_sg_id privsub=$priv_subnet_id1 pubsub=$pub_subnet_id \
pubsub2=$priv_subnet_id2 sgname=$kube_sg_name" create.yml

ssh -i /var/lib/jenkins/${KeyFile} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ec2-user@$(grep -A1 '^\[controller' $ANSIBLEENV/hosts  | tail -1 | awk '{print $1}') "cd kubespray; ansible-playbook -i inventory/mycluster/inventory.ini --become --become-user=root cluster.yml"

ansible-playbook -i $ANSIBLEENV --extra-vars "ansible_ssh_private_key_file="/var/lib/jenkins/${KeyFile}" chosen_region=$region teamname=$teamname dns_zone=$dns_domain project_name=$project \
owner_name=$owner start_date=$start_date end_date=$end_date zone_id=$zone_id vpcid=$vpc_id vpcsg=$vpc_sg_id privsub=$priv_subnet_id1 pubsub=$pub_subnet_id \
pubsub2=$priv_subnet_id2 sgname=$kube_sg_name" postsetup.yml

rm -f /var/lib/jenkins/${KeyFile}
