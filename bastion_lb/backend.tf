terraform {
  backend "s3" {
    key     = "bastion_lb/terraform.tfstate"
    encrypt = true
  }
}