#Load balancer for Bastion
resource "aws_elb" "bastion_lb" {
  name            = "${var.teamname}-LB-Bastion-${var.workspace}"
  security_groups = [data.terraform_remote_state.infra.outputs.bastion_lb_sg_id]
  subnets         = data.terraform_remote_state.infra.outputs.pub_subnet_id

  listener {
    instance_port     = 22
    instance_protocol = "tcp"
    lb_port           = 22
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:22"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 120
  connection_draining         = true
  connection_draining_timeout = 120

  tags = {
    Name       = "${var.teamname}-Loadbalancer Bastion-${var.workspace}"
    Info       = "The Loadbalancer for Bastion Host"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

#Launch Configuration for Bastion
resource "aws_launch_configuration" "laconf_bastion" {
  name                 = "${var.teamname}-LaunchConfig Bastion-${var.workspace}"
  image_id             = var.image_id[var.region]
  instance_type        = var.instance_type
  iam_instance_profile = "S3AccessProfile"
  key_name             = var.keyname
  security_groups      = [data.terraform_remote_state.infra.outputs.bastion_sg_id, data.terraform_remote_state.infra.outputs.bastion_ghost_sg_id]
  user_data            = <<EOF
        #!/bin/bash -xv
        yum -y install mysql jq
        aws s3 cp s3://${var.s3_bucket_name}/petclinic/db/schema.sql .
        aws s3 cp s3://${var.s3_bucket_name}/petclinic/db/data.sql .
        db_pass=$(aws s3 cp s3://${var.s3_bucket_name}/dbcreds/dbpass.txt -)
        rds_dns=$(aws s3 cp s3://${var.s3_bucket_name}/env:/${var.workspace}/rds/terraform.tfstate - | jq -r '.outputs.shc_rds_dns.value')
        mysql -h $rds_dns -u petclinic -p$db_pass < schema.sql
        mysql -h $rds_dns -u petclinic -p$db_pass < data.sql
        rm *.sql
        cat > /usr/bin/connect <<- '_END_'
        #!/bin/bash -xv
        PS3="Please choose the service you would like to connect to: "
        select service in PetClinic Database Jenkins Registry
        do
        case $service in
            PetClinic)
                aws s3 cp s3://${var.s3_bucket_name}/keypairs/user_${var.workspace}.pem ~/.ssh/user_${var.workspace}.pem
                chmod 600 ~/.ssh/user_${var.workspace}.pem
                echo "Connecting to $service... "
                sleep 2
                ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ~/.ssh/user_${var.workspace}.pem ec2-user@$petclinic.${var.workspace}.shc.academy.grads.al-labs.co.uk
                rm ~/.ssh/user_${var.workspace}.pem
                ;;
            Database)
                echo "Connecting to $service... "
                sleep 2
                db_pass=$(aws s3 cp s3://${var.s3_bucket_name}/dbcreds/dbpass.txt -)
                rds_dns=$(aws s3 cp s3://${var.s3_bucket_name}/env:/${var.workspace}/rds/terraform.tfstate - | jq -r '.outputs.shc_rds_dns.value')
                mysql -h $rds_dns -u petclinic -p$db_pass petclinic
                ;;
            Jenkins)
                aws s3 cp s3://${var.s3_bucket_name}/keypairs/user_${var.workspace}.pem ~/.ssh/user_${var.workspace}.pem
                chmod 600 ~/.ssh/user_${var.workspace}.pem
                echo "Connecting to $service... "
                sleep 2
                ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ~/.ssh/user_${var.workspace}.pem ec2-user@jenkins.shc.academy.grads.al-labs.co.uk
                rm ~/.ssh/user_${var.workspace}.pem
                ;;
            Registry)
                aws s3 cp s3://${var.s3_bucket_name}/keypairs/user_${var.workspace}.pem ~/.ssh/user_${var.workspace}.pem
                chmod 600 ~/.ssh/user_${var.workspace}.pem
                echo "Connecting to $service... "
                sleep 2
                ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ~/.ssh/user_${var.workspace}.pem ec2-user@shc-docker-reg.academy.grads.al-labs.co.uk
                rm ~/.ssh/user_${var.workspace}.pem
                ;;
            *)
                echo "Invalid Option. Please try again."
                break
                ;;
        esac
        done
_END_
        chmod +x /usr/bin/connect
  EOF

  lifecycle {
    create_before_destroy = true
  }
}

#ASG for Bastion
resource "aws_autoscaling_group" "asg_bastion" {
  name                      = "${var.teamname}-ASG Bastion-${var.workspace}"
  max_size                  = 3
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 1
  force_delete              = true
  launch_configuration      = aws_launch_configuration.laconf_bastion.name
  vpc_zone_identifier       = data.terraform_remote_state.infra.outputs.pub_subnet_id
  load_balancers            = [aws_elb.bastion_lb.name]

  tag {
    key                 = "Name"
    value               = "${var.teamname}-Bastion Host-${var.workspace}"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }

}

#ASG Attachment for Bastion
resource "aws_autoscaling_attachment" "asg_attachment_bastion" {
  autoscaling_group_name = aws_autoscaling_group.asg_bastion.id
  elb                    = aws_elb.bastion_lb.id
}

#Route53 for Bastian - redirects ssh to the bastion instance
resource "aws_route53_record" "www-bastion" {
  zone_id = var.zone_id
  name    = "bastion-${var.teamname}.academy.grads.al-labs.co.uk"
  type    = "CNAME"
  ttl     = "60"
  records = [aws_elb.bastion_lb.dns_name]
}
