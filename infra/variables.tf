variable "teamname" {
  type    = string
  default = ""
}

variable "owner" {
  type    = string
  default = ""
}

variable "project" {
  type    = string
  default = ""
}

variable "start_date" {
  type    = string
  default = ""
}

variable "end_date" {
  type    = string
  default = ""
}

variable "region" {
  type    = string
  default = ""
}

variable "keyname" {
  type    = string
  default = ""
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "image_id" {
  type = map
  default = {
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
    eu-west-2 = "ami-08b993f76f42c3e2f"
  }
}

variable "azs" {
  type    = list(string)
  default = ["us-west-2a", "us-west-2b", "us-west-2c"]
}

variable "public_subnet_cidr" {
  type    = string
  default = "10.0.0.0/24"
}

variable "public_subnet_cidr2" {
  type    = string
  default = "10.0.2.0/24"
}

variable "private_subnet_cidr" {
  type    = string
  default = "10.0.1.0/24"
}

variable "private_subnet_cidr2" {
  type    = string
  default = "10.0.3.0/24"
}

variable "workspace" {
  type    = string
  default = "default"
}

variable "hassanip" {
  type    = string
  default = "82.37.149.182/32"
}

variable "szymonip" {
  type    = string
  default = "86.10.108.213/32"
}

variable "cameronip" {
  type    = string
  default = "81.100.212.63/32"
}

variable "s3_bucket_name" {
  type    = string
  default = "shc-ass4"
}

variable "bb_api" {
  type    = list(string)
  default = ["104.192.136.0/21", "185.166.140.0/22", "18.205.93.0/25", "18.234.32.128/25", "13.52.5.0/25", "82.37.149.182/32", "86.10.108.213/32", "81.100.212.63/32"]
}