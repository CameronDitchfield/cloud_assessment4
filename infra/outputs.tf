output "pub_subnet_id" {
  value       = aws_subnet.public_subnet.*.id
  description = "Public Subnet ID"
}

output "priv_subnet_id" {
  value       = aws_subnet.private_subnet.*.id
  description = "Private Subnet ID"
}

output "priv_subnet_id2" {
  value       = aws_subnet.private_subnet2.*.id
  description = "Private Subnet 2 ID"
}

output "vpc_id" {
  value       = aws_vpc.vpc.id
  description = "The VPC ID created"
}

output "nat_id" {
  value       = aws_nat_gateway.nat.id
  description = "The NAT ID created"
}

output "vpc_sg_id" {
  value       = aws_vpc.vpc.default_security_group_id
  description = "The VPC Security Group created"
}


output "bastion_ghost_sg_id" {
  value       = aws_security_group.bastion_ghost_sg.id
  description = "The Bastion Ghost Security Group ID"
}

output "bastion_sg_id" {
  value       = aws_security_group.bastion_sg.id
  description = "The Bastion Security Group ID"
}

output "db_sg_id" {
  value       = aws_security_group.DB_SG.id
  description = "The Database Security Group ID"
}

output "jenkins_sg_name" {
  value       = aws_security_group.jenkins_sg.name
  description = "The Jenkins Security Group Name"
}

output "registry_sg_id" {
  value       = aws_security_group.registry_sg.id
  description = "The Docker Registry Security Group ID"
}

output "bastion_lb_sg_id" {
  value       = aws_security_group.bastion_lb_sg.id
  description = "The Bastion Loadbalancer Security Group ID"
}

output "kube_sg_id" {
  value       = aws_security_group.kube_sg.id
  description = "The Kubernetes Cluster Security Group ID"
}

output "kube_sg_name" {
  value       = aws_security_group.kube_sg.name
  description = "The Kubernetes Cluster Security Group Name"
}

output "vpc_default_route_table_id" {
  value       = aws_vpc.vpc.default_route_table_id
  description = "The Default Route Table for VPC"
}

output "aws_internet_gateway_id" {
  value       = aws_internet_gateway.igw.id
  description = "The Internet Gateway ID"
}

output "rds_subnet_grp_id" {
  value       = aws_db_subnet_group.rds_snet_grp.id
  description = "The RDS DB Subnet Group ID"
}
