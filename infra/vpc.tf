# VPC
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name       = "${var.teamname}-VPC-${var.workspace}"
    Info       = "The VPC"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Default route table for vpc
resource "aws_default_route_table" "r" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  tags = {
    Name       = "${var.teamname}-Default VPC Route Table-${var.workspace}"
    Info       = "The Default VPC Routing Table"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name       = "${var.teamname}-IGW-${var.workspace}"
    Info       = "The Internet Gateway"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}