# Public Subnet
resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet_cidr
  availability_zone       = "${var.region}a"
  map_public_ip_on_launch = true

  tags = {
    Name       = "${var.teamname}-Public Subnet-${var.workspace}"
    Info       = "The Public Subnet"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Route Table for public subnet
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name       = "${var.teamname}-Public Route Table-${var.workspace}"
    Info       = "The Public Subnet Routing Table"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Route table association with public subnet
resource "aws_route_table_association" "public_ta" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public_rt.id
}

# Private Subnet
resource "aws_subnet" "private_subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_subnet_cidr
  availability_zone       = "${var.region}a"
  map_public_ip_on_launch = false

  tags = {
    Name       = "${var.teamname}-Private Subnet-${var.workspace}"
    Info       = "The Private Subnet"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

resource "aws_subnet" "private_subnet2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_subnet_cidr2
  availability_zone       = "${var.region}b"
  map_public_ip_on_launch = false

  tags = {
    Name       = "${var.teamname}-Private Subnet 2-${var.workspace}"
    Info       = "The Private Subnet 2"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Route Table for private subnet
resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat.id
  }

  tags = {
    Name       = "${var.teamname}-Private Route Table-${var.workspace}"
    Info       = "The Private Subnet Routing Table"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Route table association private with vpc main route table
resource "aws_route_table_association" "private_ta" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private_rt.id
}

# Database subnet group for the RDS instance
resource "aws_db_subnet_group" "rds_snet_grp" {
  name       = "rds_snet_grp"
  subnet_ids = [aws_subnet.private_subnet.id, aws_subnet.private_subnet2.id]
  tags = {
    Name       = "${var.teamname}-DB subnet group-${var.workspace}"
    Info       = "The DB Subnet Group"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}