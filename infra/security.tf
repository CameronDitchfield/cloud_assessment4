# Empty Security Group for Bastion Host - for SSH onto private network
resource "aws_security_group" "bastion_ghost_sg" {
  name        = "${var.teamname}-Bastion Ghost SG-${var.workspace}"
  description = "Bastion Ghost Security Group"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name       = "${var.teamname}-Bastion Ghost SG-${var.workspace}"
    Info       = "The Bastion Ghost Security Group"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Security Group for SSH onto Bastion - allow only SSH from team, within vpc, and jenkins
resource "aws_security_group" "bastion_sg" {
  name        = "${var.teamname}-Bastion SG-${var.workspace}"
  description = "Bastion Security Group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.hassanip, var.szymonip, var.cameronip, var.vpc_cidr]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.jenkins_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name       = "${var.teamname}-Bastion SG-${var.workspace}"
    Info       = "The Bastion Security Group"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Security Group for the database - Allow traffic on VPC subnets to access DB
resource "aws_security_group" "DB_SG" {
  name        = "${var.teamname}-vpc_db-${var.workspace}"
  description = "DB Security Group"

  ingress {
    from_port   = 0
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [var.hassanip, var.szymonip, var.cameronip, var.vpc_cidr, ]
  }

  ingress {
    from_port       = 0
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_ghost_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.vpc.id

  tags = {
    Name       = "${var.teamname}-DB SG-${var.workspace}"
    Info       = "The Database Security Group"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Security Group for Jenkins - Allow incoming Web connections.
resource "aws_security_group" "jenkins_sg" {
  name        = "${var.teamname}-Jenkins-SG-${var.workspace}"
  description = "Jenkins Security Group"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = var.bb_api
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.hassanip, var.szymonip, var.cameronip]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_ghost_sg.id]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.vpc.id

  tags = {
    Name       = "${var.teamname}-Jenkins SG-${var.workspace}"
    Info       = "The Database Security Group"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Security Group for the Docker Registry
resource "aws_security_group" "registry_sg" {
  name        = "${var.teamname}-Docker Registry SG-${var.workspace}"
  description = "The Docker Registry Security Group"

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_ghost_sg.id]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.vpc.id

  tags = {
    Name       = "${var.teamname}-Docker Registry SG-${var.workspace}"
    Info       = "The Docker Registry Security Group"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# SG for Bastion Loadbalancer
resource "aws_security_group" "bastion_lb_sg" {
  name        = "${var.teamname}-Bastion LB SG- ${var.workspace}"
  description = "Bastion Loadbalancer Security Group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.hassanip, var.szymonip, var.cameronip]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.jenkins_sg.id]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = [var.vpc_cidr]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_ghost_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name       = "${var.teamname}-Bastion LB SG-${var.workspace}"
    Info       = "The Bastion Loadbalancer Security Group"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}

# Security Group for the Kube Cluster
resource "aws_security_group" "kube_sg" {
  name        = "${var.teamname}-Kube-Cluster-SG-${var.workspace}"
  description = "The Kubernetes Cluster Security Group"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.vpc.id

  tags = {
    Name       = "${var.teamname}-Kube Cluster SG-${var.workspace}"
    Info       = "The Kubernetes Cluster Security Group"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
}
