#!/bin/bash

if (( $# < 2 ))
then
  echo "SYNTAX: $0 <aws_profile> <aws_region>" 1>&2
  exit 1
else 
  AWS_PROFILE=$1
  AWS_REGION=$2
fi

AWS_ACCESS_KEY=$(grep -A2 "\[$AWS_PROFILE\]" ~/.aws/credentials | grep -i aws_access | awk '{print $3}' | sed 's/  *//g')
AWS_SECRET_KEY=$(grep -A2 "\[$AWS_PROFILE\]" ~/.aws/credentials | grep -i aws_secret | awk '{print $3}' | sed 's/  *//g')
AWS_SESSION_TOKEN=$(grep -A3 "\[$AWS_PROFILE\]" ~/.aws/credentials | grep -i aws_session | awk '{print $3}' | sed 's/  *//g')
export AWS_ACCESS_KEY AWS_SECRET_KEY AWS_SESSION_TOKEN AWS_PROFILE AWS_REGION

