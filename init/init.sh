#!/bin/bash
# Builds the jenkins machine locally,
# then launches the terraform job to build the rest of the infrastructure

# Source values from Configuration file and Set Environment
source var/shc.tfvars
source init/set_env.sh $profile $region

# Make s3 bucket
if aws s3api list-buckets --query "Buckets[].Name" | grep "$bucket_name"
then
  echo "S3 bucket with name $bucket_name already exists!"
else
  ./init/s3bucket.sh $bucket_name $aws_accid $profile $region
  echo "Created S3 bucket"
fi

# Make SSH keys and send them to the s3 bucket
if aws ec2 describe-key-pairs --key-name user_${env}
then
  echo "SSH keys have already been created!"
else
  ./init/keys.sh
  echo "Created and uploaded SSH key pair"
fi

 # Launch Terraform Infrastructure
cd infra
terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$region"
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=../var/shc.tfvars -auto-approve
then
    echo "Infrastructure created successfully"
else
    echo "Infrastructure build failed - cleaning up"
    terraform destroy --var-file=../var/shc.tfvars -auto-approve
    exit 1
fi

#Extract output variables
chosen_SG=$(terraform output jenkins_sg_name)
chosen_subnet=$(terraform output pub_subnet_id | grep -o '".*"' | tr -d '"')

# Launch Jenkins
cd ../jenkins_instance
ansible-playbook -i environments/${env} instance.yml --extra-vars "ansible_ssh_private_key_file=../user_${env}.pem \
chosen_region=$region bucket=$bucket_name teamname=$teamname pb_dir=$PWD dns_domain=$dns_domain project_name=$project \
owner_name=$owner start_date=$start_date end_date=$end_date chosen_SG=$chosen_SG chosen_subnet=$chosen_subnet env=$env"

# Launch RDS
cd ../rds
terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$region"
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=../var/shc.tfvars -auto-approve
then
    echo "RDS created successfully"
else
    echo "RDS build failed - cleaning up"
    terraform destroy --var-file=../var/shc.tfvars -auto-approve
    exit 2
fi

# Launch Registry
cd ../registry
terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$region"
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=../var/shc.tfvars -auto-approve
then
    echo "Registry created successfully"
else
    echo "Registry build failed - cleaning up"
    terraform destroy --var-file=../var/shc.tfvars -auto-approve
    exit 2
fi

# Launch Bastion Host
cd ../bastion_lb
terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$region"
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=../var/shc.tfvars -auto-approve
then
    echo "Bastion Host created successfully"
else
    echo "Bastion build failed - cleaning up"
    terraform destroy --var-file=../var/shc.tfvars -auto-approve
    exit 2
fi

# Remove keypair .pem file from initial creation - if need have to download from S3
cd ..
rm *.pem

# END OF BUILD
echo " SYSTEM READY"
