#!/bin/bash

if (( $# < 4 ))
then
  echo "$0 <bucket_name> <aws_account_id> <aws_profile> <aws_region>"
  exit 1
else 
  BUCKET_NAME=$1
  AWS_ACCID=$2
  source init/set_env.sh $3 $4
fi

source init/config.sh
IAM_ROLE=242392447408:role/ALAcademyJenkinsSuperRole
# 242392447408

cat > s3policy.json <<_END
{
    "Version":"2012-10-17",
    "Statement":[{
        "Sid":"AddAWSAccounts",
        "Effect":"Allow",
        "Principal": {"AWS": [
            "arn:aws:iam::$AWS_ACCID:root",
            "arn:aws:iam::$AWS_ACCID:assumed-role/OrganizationAccountAccessRole/hassan@automationlogic.com",
            "arn:aws:iam::$AWS_ACCID:assumed-role/OrganizationAccountAccessRole/cameron@automationlogic.com",
            "arn:aws:iam::$AWS_ACCID:assumed-role/OrganizationAccountAccessRole/szymon@automationlogic.com",
            "arn:aws:iam::$IAM_ROLE"
        ]},
        "Action":["s3:*"],
        "Resource": [
            "arn:aws:s3:::$BUCKET_NAME",
            "arn:aws:s3:::$BUCKET_NAME/*"
        ]
        }]
}
_END

if aws s3 mb s3://$BUCKET_NAME --region $AWS_REGION
then 
    echo "S3 bucket $BUCKET_NAME created successfuly"
else
    echo "S3 bucket creation failed, check the name is unique"
    exit 1
fi 

if aws s3api put-bucket-policy --bucket $BUCKET_NAME \
    --policy file://s3policy.json 
then 
    echo "Bucket policy added"
else
    echo "Failed to add policy to bucket"
    exit 2
fi
rm s3policy.json

aws s3api put-public-access-block \
    --bucket $BUCKET_NAME \
    --public-access-block-configuration "BlockPublicAcls=true,\
    IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

aws s3api put-bucket-versioning \
    --bucket $BUCKET_NAME \
    --versioning-configuration Status=Enabled

if aws s3api put-bucket-tagging --bucket $BUCKET_NAME \
    --tagging TagSet="[{Key=Name,Value=$BUCKET_NAME},
        {Key=Project,Value=$TAG_PROJECT},
        {Key=Info,Value=s3-for-cloud-infra},
        {Key=Owner,Value=$TAG_OWNER},
        {Key=Start date,Value=$TAG_START},
        {Key=End date,Value=$TAG_END},
        {Key=Team,Value=$TAG_TEAM}]"
then 
    echo "Bucket tagged successfully"
else 
    echo "Failed to tag bucket"
    exit 3
fi

# Upload the petclinic schema files
aws s3 cp rds/schema.sql s3://${BUCKET_NAME}/petclinic/db/
aws s3 cp rds/data.sql s3://${BUCKET_NAME}/petclinic/db/