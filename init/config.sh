#!/bin/bash
# Configuration File for user to set up initial parameters

export region=eu-west-2                    # AWS Region
export bucket_name=shc-asstest                 # S3 Bucket Name
export aws_accid=242392447408               # AWS Account Number
export profile=academy                      # AWS Profile Name
export env=dev                              # Environment

export TAG_PROJECT=ALAcademy2020Oct
export TAG_OWNER=Szymon-Hassan-Cameron
export TAG_START=23Nov2020
export TAG_END=04Dec2020
export TAG_TEAM=$TAG_OWNER
