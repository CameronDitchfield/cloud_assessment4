#!/bin/bash

source var/shc.tfvars

#Creates key pairs and moves to S3 bucket
aws ec2 create-key-pair --key-name user_dev | jq -r '.KeyMaterial' >user_dev.pem
chmod 600 user_dev.pem
aws s3 cp user_dev.pem s3://$bucket_name/keypairs/user_dev.pem

aws ec2 create-key-pair --key-name user_prod | jq -r '.KeyMaterial' >user_prod.pem
chmod 600 user_prod.pem
aws s3 cp user_prod.pem s3://$bucket_name/keypairs/user_prod.pem
