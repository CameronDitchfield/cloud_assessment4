#!/bin/bash

#Connect script to SSH directly onto the Bastion Host

source var/shc.tfvars
source init/set_env.sh $profile $region

echo "Connecting to Bastion Host. Please Wait... "
sleep 2
if aws s3 cp s3://$bucket_name/keypairs/user_$env.pem ~/.ssh/user_$env. 2>/dev/null
then 
    chmod 600 ~/.ssh/user_$env.pem 2>/dev/null
    sleep 2
    if ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ~/.ssh/user_$env.pem ec2-user@bastion-shc.academy.grads.al-labs.co.uk 2>/dev/null
    then 
        rm ~/.ssh/user_$env.pem
        echo "Successfully connected to the Bastion Host"
    else
        echo "Unable to connect to Bastion Host. Please wait a few moments for the Bastion to initialise and try again. Please ensure credentials and environment have been configured."
        exit 1
    fi
else
    echo "Unable to connect to the S3 bucket. Please ensure credentials and environment have been configured"
    exit 1
fi