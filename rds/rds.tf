resource "aws_db_instance" "shc_rds" {
  allocated_storage = 8
  storage_type      = "gp2"
  instance_class    = "db.t2.micro"
  engine            = "mysql"
  engine_version    = "8.0.20"
  identifier        = "shc-rds"
  name              = "shc_rds"
  tags = {
    Name       = "shc-rds-${var.workspace}"
    Info = "The RDS Database for assessment 4"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }
 
  username = "petclinic"
  password = "petclinic"

  apply_immediately         = true
  final_snapshot_identifier = "shc-rds-snapshot"
  skip_final_snapshot       = true
  multi_az                  = true
  db_subnet_group_name      = data.terraform_remote_state.infra.outputs.rds_subnet_grp_id
  vpc_security_group_ids    = [data.terraform_remote_state.infra.outputs.db_sg_id]
}

resource "aws_route53_record" "shc_rds" {
  zone_id = var.zone_id
  name = "shc-rds.${var.dns_domain}"
  type = "CNAME"
  ttl = "60"
  records = [trim(aws_db_instance.shc_rds.endpoint, ":3306")]
}