output "shc_rds_id" {
  value       = aws_db_instance.shc_rds.id
  description = "The ID of the shc_rds instance"
}

output "khc_rds_endpoint" {
  value       = aws_db_instance.shc_rds.endpoint
  description = "The endpoint of the shc_rds instance"
}

output "shc_rds_dns" {
  value = aws_route53_record.shc_rds.name
  description = "The Route53 CNAME address for the RDS"
}