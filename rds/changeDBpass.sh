#!/bin/bash
#Script to change DB Password and store in the S3 bucket.

#Generate a random string to use as DB password and source configuration file for parameters
source var/shc.tfvars
openssl rand -base64 12 -out dbpass.txt
chmod 600 dbpass.txt
db_pass=$(cat dbpass.txt)

# Retrieve parameters from S£ and modify db instance password
if rds_name=$(aws s3 cp s3://$bucket_name/env:/$env/rds/terraform.tfstate - | jq -r '.outputs.shc_rds_id.value')
then
    if rdsmod=$(aws rds modify-db-instance --db-instance-identifier $rds_name \
                --master-user-password $db_pass --region $region --preferred-backup-window 06:45-07:15 \
                --preferred-maintenance-window mon:06:00-mon:06:30 --backup-retention-period 3 \
                --apply-immediately 2>/dev/null)
    then
        aws s3 cp dbpass.txt s3://$bucket_name/dbcreds/dbpass.txt 2>/dev/null
        rm dbpass.txt
        echo "The database credentials have successfully updated"
    else
        echo "Unable to update database credentials. Please ensure credentials and environment have been configured"
        rm dbpass.txt
        exit 1
    fi
else
    echo "Unable to connect to S3 bucket. Please ensure credentials and environment have been configured"
    rm dbpass.txt
    exit 1
fi
