variable "teamname" {
  type    = string
  default = ""
}

variable "keyname" {
  type    = string
  default = ""
}

variable "owner" {
  type    = string
  default = ""
}

variable "project" {
  type    = string
  default = ""
}

variable "start_date" {
  type    = string
  default = ""
}

variable "end_date" {
  type    = string
  default = ""
}

variable "workspace" {
  type = string
}

variable "region" {
  type    = string
  default = ""
}

variable "dns_domain" {
  type = string
  default = "academy.grads.al-labs.co.uk"
}

variable "zone_id" {
  type = string
  default = "Z07626429N74Z31VDFLI"
}

variable "s3_bucket_name" {
  type    = string
  default = "shc-ass4"
}