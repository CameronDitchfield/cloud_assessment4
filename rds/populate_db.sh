#!/bin/bash
# Script to populate database if required

source var/shc.tfvars
source init/set_env.sh $profile $region

db_pass=$(aws s3 cp s3://$bucket_name/dbcreds/dbpass.txt - 2>/dev/null)
rds_dns=$(aws s3 cp s3://$bucket_name/env:/$env/rds/terraform.tfstate - | jq -r '.outputs.shc_rds_dns.value')

aws s3 cp s3://$bucket_name/petclinic/db/schema.sql .
aws s3 cp s3://$bucket_name/petclinic/db/data.sql .

mysql -h $rds_dns -u petclinic -p$db_pass < schema.sql
mysql -h $rds_dns -u petclinic -p$db_pass < data.sql

rm *.sql