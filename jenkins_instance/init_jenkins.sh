#!/bin/bash

source var/shc.tfvars
source init/set_env.sh $profile $region

#Download ssh key
KeyFile="user_${env}.pem"
aws s3 cp s3://${bucket_name}/keypairs/${KeyFile} ./${KeyFile}
chmod 600 ./${KeyFile}

#Grab terraform output variables
cd ../infra
chosen_SG=$(terraform output jenkins_sg_name)
chosen_subnet=$(terraform output pub_subnet_id | grep -o '".*"' | tr -d '"')

#Run playbook
cd ../jenkins_instance
ansible-playbook -i environments/${env} instance.yml --extra-vars "ansible_ssh_private_key_file=../user_${env}.pem \
chosen_region=$region bucket=$bucket_name teamname=$teamname pb_dir=$PWD dns_domain=$dns_domain project_name=$project \
owner_name=$owner start_date=$start_date end_date=$end_date chosen_SG=$chosen_SG chosen_subnet=$chosen_subnet env=$env"

#Remove keyfile
rm -f ./${KeyFile}
