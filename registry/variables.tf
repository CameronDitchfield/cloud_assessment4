variable "workspace" {
  type    = string
  default = ""
}

variable "teamname" {
  type    = string
  default = ""
}

variable "owner" {
  type    = string
  default = ""
}

variable "project" {
  type    = string
  default = ""
}

variable "start_date" {
  type    = string
  default = ""
}

variable "end_date" {
  type    = string
  default = ""
}
variable "region" {
  type    = string
  default = ""
}

variable "keyname" {
  type    = string
  default = ""
}

variable "image_id" {
  type = map
  default = {
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
    eu-west-2 = "ami-08b993f76f42c3e2f"
  }
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "dns_domain" {
  type = string
  default = "academy.grads.al-labs.co.uk"
}

variable "zone_id" {
  type = string
  default = "Z07626429N74Z31VDFLI"
}

variable "s3_bucket_name" {
  type    = string
  default = "shc-ass4"
}