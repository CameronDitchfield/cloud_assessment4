terraform {
  backend "s3" {
    key     = "docker-reg/terraform.tfstate"
    encrypt = true
  }
}