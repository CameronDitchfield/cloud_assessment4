data "terraform_remote_state" "infra" {
  backend = "s3"
  config = {
    bucket = var.s3_bucket_name
    key    = "env:/${var.workspace}/infra/terraform.tfstate"
    region = var.region
  }
}