output "shc_docker_reg_id" {
  value       = aws_instance.shc_docker_reg.id
  description = "The id of the khc_rds instance"
}

# output "shc_docker_reg_priv_dns" {
#   value = aws_instance.shc_docker_reg.private_dns
#   description = "The private DNS of the docker registry instance"
# }

output "shc_docker_reg_dns" {
  value = aws_route53_record.shc_docker_reg.name
  description = "The route53 CNAME address for the docker registry"
}