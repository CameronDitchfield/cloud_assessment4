resource "aws_instance" "shc_docker_reg" {
  ami = var.image_id[var.region]
  instance_type = var.instance_type
  key_name = var.keyname
  vpc_security_group_ids = [data.terraform_remote_state.infra.outputs.registry_sg_id]
  subnet_id = data.terraform_remote_state.infra.outputs.priv_subnet_id[0]
  # vpc_security_group_ids = ["cameron_test_vpc_sg"]
  tags = {
    Name       = "shc-docker-reg-${var.workspace}"
    Info       = "Private Docker registry for assessment 4"
    Project    = var.project
    Owner      = var.owner
    Start_Date = var.start_date
    End_Date   = var.end_date
    Team       = var.owner
  }

  root_block_device {
    volume_size = 12
  }
  
  user_data = <<EOF
    #!/bin/bash -xv
    yum -y install docker
    systemctl start docker
    systemctl enable docker
    docker run -d -p 5000:5000 --restart=always --name registry -it -v /var/lib/dockerreg:/var/lib/registry registry:2.7.1
  EOF
}

resource "aws_route53_record" "shc_docker_reg" {
  zone_id = var.zone_id
  name = "shc-docker-reg.${var.dns_domain}"
  type = "CNAME"
  ttl = "60"
  records = [aws_instance.shc_docker_reg.private_dns]
}
