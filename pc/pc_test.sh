#!/bin/bash

PC_ADDR=localhost

while (( cnt < 20 ))
do
  if (( $(curl -s ${PC_ADDR}:1080 | wc -l) > 0 ))
  then
    echo "Petclinic is running"
    break
  fi

  (( cnt = cnt + 1 ))
  sleep 30

  if (( cnt == 20 ))
  then
    echo "WARNING ----- Petclinic compile and launch failed"
    exit 1
  fi
done

if (( $(curl -I ${PC_ADDR}:1080/owners?lastName= 2>/dev/null | head -n 1 | cut -d$' ' -f2) == 200 ))
then
  echo "Petclinic and DB launched successfully"
else 
  echo "WARNING ----- Petclinic cannot communicate with the database"
  exit 1
fi