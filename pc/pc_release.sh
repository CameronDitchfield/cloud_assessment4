#!/bin/bash

if (( $# < 2 ))
then
  echo "usage: $0 <registry_dns> <app_version>"
else 
  REG_DNS=$1
  VER=$2
fi

docker tag petclinic petclinic:latest
docker tag petclinic:latest $REG_DNS:5000/petclinic:$VER
docker tag petclinic:latest $REG_DNS:5000/petclinic:latest
echo "Docker tag successful"

docker push $REG_DNS:5000/petclinic:$VER
docker push $REG_DNS:5000/petclinic:latest
echo "Petclinic image version: $VER successfully pushed to the docker registry"