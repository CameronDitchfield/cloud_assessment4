#!/bin/bash
mkdir -p /opt/petclinic/config
cp /app/petclinic.jar /opt/petclinic/

cat > /opt/petclinic/config/application.properties << '_END_'
database=mysql
spring.datasource.url=jdbc:mysql://pc-db:3306/${DB_NAME}
spring.datasource.username=${DB_USER}
spring.datasource.password=${DB_PASSWORD}
spring.jpa.hibernate.ddl-auto=none
logging.level.org.springframework=INFO
spring.profiles.active=production
_END_

while ! nc -z -w3 $DB_SERVERNAME 3306
do 
  sleep 5
done

cd /opt/petclinic
java -jar petclinic.jar