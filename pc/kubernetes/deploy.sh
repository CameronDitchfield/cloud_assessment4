#!/bin/bash -xv

if (( $# < 3 ))
then
  echo "usage: $0 <environment> <bucket_name> <aws_region>"
else 
  WS=$1
  BUCKET_NAME=$2
  REGION=$3
fi

SSH_ARGS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

# Download key
aws s3 cp s3://$BUCKET_NAME/keypairs/user_$WS.pem user_$WS.pem
chmod 600 user_$WS.pem

ls -l

# Get private IPs of k8s controller and master
controller_ip=$(aws ec2 describe-instances --filters Name=tag:Name,Values=$WS-shc-Kubernetes-Controller --region=$REGION | jq -r '.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress')
master_ip=$(aws ec2 describe-instances --filters Name=tag:Name,Values=$WS-shc-Kubernetes-Master --region=$REGION | jq -r '.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress')

# Checkout repo on controller
ssh -i user_$WS.pem $SSH_ARGS ec2-user@$controller_ip bash <<EOF
git clone https://bitbucket.org/CameronDitchfield/cloud_assessment4.git
cd cloud_assessment4
git checkout cameron_dev
EOF
# ^^^^^^^^^^^^^^^^^^^CHANGE TO MASTER^^^^^^^^^^^^^^^^^^

# Updated deployment config with latest db password
./update_env.sh $BUCKET_NAME

# scp deployment config to controller
scp -i user_$WS.pem $SSH_ARGS petclinic-deployment.yaml ec2-user@$controller_ip:cloud_assessment4/pc/kubernetes/

# Deploy petclinic in cluster
ssh -i user_dev.pem $SSH_ARGS ec2-user@$controller_ip bash <<EOF
cd cloud_assessment4/pc/kubernetes
kubectl create namespace $WS
kubectl apply -f ingress.yml -n $WS
kubectl apply -f petclinic-deployment.yaml -n $WS
kubectl apply -f petclinic-service.yaml -n $WS
kubectl apply -f pc-db-service.yaml -n $WS
EOF

# Get port number
# kubectl get all -n haproxy-controller
# Test connection
# curl -I -H 'Host: petclinic.dev.shc.academy.grads.al-labs.co.uk' 'http://$master_ip:31904'

# Remove key
#rm user_$WS.pem
