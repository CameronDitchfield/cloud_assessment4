#!/bin/bash

if (( $# < 1 ))
then
  echo "usage: $0 <s3_bucket_name>"
else 
  BUCKET_NAME=$1
fi

db_pass=$(aws s3 cp s3://$BUCKET_NAME/dbcreds/dbpass.txt -)

cat > petclinic-deployment.yaml <<'_END_'
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    kompose.cmd: kompose convert
    kompose.version: 1.22.0 (955b78124)
  creationTimestamp: null
  labels:
    io.kompose.service: petclinic
  name: petclinic
spec:
  replicas: 1
  selector:
    matchLabels:
      io.kompose.service: petclinic
  strategy: {}
  template:
    metadata:
      annotations:
        kompose.cmd: kompose convert
        kompose.version: 1.22.0 (955b78124)
      creationTimestamp: null
      labels:
        io.kompose.service: petclinic
    spec:
      containers:
        - env:
            - name: DB_NAME
              value: petclinic
            - name: DB_PASSWORD
              value: $db_pass
            - name: DB_SERVERNAME
              value: pc-db
            - name: DB_USER
              value: petclinic
          image: petclinic
          name: petclinic
          ports:
            - containerPort: 8080
          resources: {}
      restartPolicy: Always
status: {}
_END_