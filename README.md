# Cloud Assessment 4

**Szymon, Hassan and Cameron** \
This ReadMe file contains documentation that will assist with the use of our code.

##  Contents
* Prerequisites
* Installation
* Automation Server
* Bitbucket webhook
* Kubernetes Cluster

##  Prerequisites

**PYTHON3**
* Make sure you have the current version of Python3 installed on your machine.
* Install PIP
* Install packages from requirements.txt (using virtual environment is highly recommended)

**AWS**
* Downloaded credentials file

## Installation
* Check out the repository into your home directory:
>    
    $ git clone https://bitbucket.org/CameronDitchfield/cloud_assessment4.git


* PLEASE EDIT THE CONFIGURATION FILE WITH USER PARAMETERS:
> The user must edit the *var/shc.tfvars* file and alter the initial parameters such as AWS Account ID, AWS Profile Name, Region, Tags etc.
 * MAKE SURE TO PUSH YOUR CONFIGURED TFVARS FILE TO THE MASTER BRANCH OF THE REPOSITORY.
 
><sub>You might want to set *ansible_python_interpreter* variable inside *jenkins_instance/environments/env/group_vars/local* to point to the Python interpreter which you are currently using.<sub>

* Execute the *init.sh* script from directly inside the *cloud_assessment4* folder by the following on the CLI (command line interface):
>   
    $ cd cloud_assessment4/ \
    $ ./init/init.sh  \
> IMPORTANT: Please ensure the user is logged on via the aws console and have downloaded their most recent aws **'credentials'** file on their local machine, as this is required to set-up the working environment. If an error occurs during this step, please ensure that the aws 'credentials' is locally downloaded and then run the *set_env.sh* script to ensure that the environment is appropriately configured.

This script will use terraform to create network infrastructure, RDS (relational database service), private docker registry, and bastion host. On top of that Jenkins instance will be launched and provisioned using ansible. SSH key pair as well as S3 bucket will also be created.

* Log on to the Jenkins and run the jobs in the following order:
 * build_K8sCluster
 * petclinic
 * deploy_petclinic

## Automation Server

Jenkins plays the role of an automation server for this CI/CD pipeline. It comes with pre-installed:
* Terraform
* Ansible
* Docker
* GIT
* Java
* Kubectl
* MariaDB

By default Jenkins can be accessed at:
>http://jenkins.shc.academy.grads.al-labs.co.uk:8080

Default Login details:
>Username: admin <br>Password: admin

Jenkins Ansible Playbook variables can be easily changed within:
>* */jenkins_instance/environments/ENV/group_vars/all*  &
>* */jenkins_instance/environments/ENV/group_vars/local*.

In case of any problems with Jenkins, the server can be taken down by running:
>
    $ cd jenkins_instance
    $ ./destroy_jenkins.sh

Likewise it can be set up again by running:
>
    $ cd jenkins_instance
    $ ./init_jenkins.sh

Always make sure to check if the inventory is empty before recreating Jenkins

#### BITBUCKET WEBHOOK
A web-hook to build the Jenkins jobs has been partly created for this BitBucket repository, however in order for it to work successfully there are some extra steps to complete. If set up, any push or merged pull request to the repository will trigger the relevant jobs to be built in Jenkins.

* Make sure that *Petclinic* job has an option *Build when a change is pushed to BitBucket* ticked in *Build Triggers* section.
* (only if you are not using the default repository) ==> The webhook should be set up in the BitBucket repository settings to link to the Jenkins server on
http://jenkins.shc.academy.grads.al-labs.co.uk:8080/bitbucket-hook/

## Kubernetes Cluster
Kubernetes has been deployed as the container-orchestration system. The cluster is initialised through the Jenkins job *build_K8sCluster*:

* It creates 1 master and 2 worker nodes which are located in the private subnet of the VPC
* The cluster has a load balancer attached to it.
* If needed the cluster can be destroy by running *destroy_K8sCluster* job and recreated by running *build_K8sCluster*

<!-- * Once the jenkins server is up and running log on to it using the DNS outputted by the init.sh script in order to make a jenkins API token
    * Once on the jenkins server log in with the credentials
        * Username: admin
        * Password: atest
    * Navigate to the 'Manage Jenkins' tab, then 'Manage Users', select the admin user and navigate to 'Configure' on the sidebar.
    * In the API Token section, select create new token, give your token a name and take a copy of the token created.
* Now return to the command line in the init directory and run the *launch.sh* script
    * This script requires two parameters;
        *  The environment to build in 'dev' or 'prod'
        *  The API token you created in the previous step
   *  This script will trigger the launch of the infrastructure, which will trigger the launch of each of the individual components of the environment. -->

## Git
* Master branch contains production code.

* Dev contains code that works and has been tested.

* User branches contain code that is still in development.

## S3 Bucket
* The S3 bucket being used for this assessment is called shc-ass4.

* The bucket is launched using the *s3bucket.sh* script.

## Connecting to Bastion Host - Management

* The management user can SSH onto the Bastion Host to further SSH/connect onto the resources within the VPC to perform any required management tasks.

* Firstly, ensure that initialisation has been carried out *init.sh*, so that the user can be granted acces to the S3 bucket and retrieve sensitive credentials.

* Ensure the following parameters are set and exported to the environment.
>
    $ export AWS_REGION=<region>
    $ export AWS_PROFILE=<profile_name>

* To SSH onto the Bastian Host, then simply run the *ssh_bastion.sh* script or by running the following command from the CLI and inside the *cloud_assessment4* directory. This will directly connect the user to the Bastion Host regardless of the deployment location.
>
    $ ./init/ssh_bastion.sh

* To exit from the Bastion Host simply write **'exit'** on the CLI.

> #### ***ERRORS***
> - If any errors occur regarding connection, identity, timeout or authorisation please ensure that the initialisation script has been ran and  the environment has been configured. To resolve this issue please ensure that the user is logged onto the AWS console, downloaded the recent credentials and then run the *'set_env.sh'* script to re-configure the environment.
> - If an error occurs regarding not resolving the hostname, please wait a few moments (~30-60 seconds) for the Bastion Host to initialise before trying to SSH again.
> - To re-configure the environment, run the following command from inside the *cloud_assessment4* directory, with the two arguments:
>
    $ . ./init/set_env.sh <profile_name> <region>

## Connecting from Bastion Host to another resource

* The Bastion Host provides a platform for the management user to SSH or connect to another resource within the VPC to perform management or admin activities.

* The user can connect to the following resources:

>1. **PetClinic**         --> the instance running behind the petclinic webserver
>2. **Database**          --> the user can log onto the database server
>3. **Jenkins**           --> the instance running the jenkins server
>4. **Docker Registry**   --> the instance running behind the docker registry

* To connect to one of the resources the user simply inputs 'connect' on the CLI, from the home directory, and then selects the resource by entering one of the corrosponding numbers:
>
    $ connect       

* To exit from a resource simply write 'exit' on the CLI which would return the user to the Bastion Host.

## VPC & Infrastructure

The infrastructure is configured through Terraform and applies the following resources to the environment:

>* Virtual Private Cloud
>* Private and Public Subnet
>* Internet Gateway
>* NAT Gateway
>* Routing Tables

## Docker Registry
The system utilises a Private Docker Registry located in the private subnet for storing any Docker images created. The registry has the static dns shc-docker-reg.academy.grads.al-labs.co.uk .

It is launched from terraform.
>Where ENV is the Environment argument  *(dev/prod)*.

## Petclinic
Petclinic is compiled, run and tested in containers on the Jenkins server (job name: *petclinic*) before being deployed in the Kubernetes cluster.

The latest version of the spring Petlinic application is compiled from [here](https://github.com/spring-projects/spring-petclinic).

Once tested, the petclinic container image is uploaded to the private docker registry tagged with both the latest and version number. THe version number tags correspond with the Jenkins $BUILD_NUMBER environment variable for the *petclinic* job.

Petclinic is deployed in the cluster by the *deploy_petclinic* Jenkins job and the instances in the cluster are accessed by the hostname petclinic.dev.shc.academy.grads.al-labs.co.uk.

## Database

The RDS is configured to automatically change and update the databse password. The allocated password is randomly generated to increase security to the data. The system is designed to generate a new password and attempt to change the databse master user password. If it a success, the password updates in the s3 bucket. However, if the job fails to update the database password a slackbot has been configure to send an alert to the selected channel. 

The databse has been scheduled to chnage the database password every 4 hours. This can be adjusted by logging onto Jenkins, as previously discussed, and editting the *change_db_password* job in the configure build area. If the databse password fails to modify then the s3 bucket does not get updated.

**POPULATE DATA INTO DATABASE**

The database is populated during the initialisation and launch of the bastion host. If further data is required to be populated into the database then the user-data of the bastion should be editted.

**BACKUP AND MAINTAINENCE WINDOW**

The backup and maintainence window of the databse is set in the *changeDBpass.sh* script and can be editted there. The backup window is currently set between 06:45-07:15 and the maintainence window is set between mon:06:00-mon:06:30.

## Slackbot

The slackbot is configured to send an alert if the system fails to change the database password. The error message, as well as any further alerts can be editting or added. This can be done by logging onto jenkins and change the end of the build configuration at the post-build stage.

